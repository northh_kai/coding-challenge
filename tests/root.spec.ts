// Dependencies
import { server } from "../src/server";
import supertest from "supertest";

const apiRequest = supertest(server);

describe("API /", () => {
  let session: string;

  beforeAll(async () => {
    const credentials = {
      email: "tom@tester.de",
      password: "Password123!",
    };

    const res = await apiRequest.post("/auth/login").send(credentials);
    session = res.body.result.session;
  });

  it("GET / should return hello world", async () => {
    const res = await apiRequest.get("/");
    expect(res.status).toEqual(200);
    expect(res.type).toEqual(expect.stringContaining("json"));
    expect(res.body).toHaveProperty("result");
    expect(res.body.result).toContain("I am a vending machine");
  });

  it("GET /protected should return 200", async () => {
    const res = await apiRequest.get("/protected?s=" + session);
    expect(res.status).toEqual(200);
    expect(res.type).toEqual(expect.stringContaining("json"));
    expect(res.body).toHaveProperty("result");
    expect(res.body.result).toContain("Protected page");
  });

  it("GET /protected should return 403 on invalid session", async () => {
    const res = await apiRequest.get("/protected?s=INVALID");
    expect(res.status).toEqual(403);
    expect(res.type).toEqual(expect.stringContaining("json"));
    expect(res.body).toHaveProperty("error");
    expect(res.body.error).toContain("Forbidden");
  });
});
