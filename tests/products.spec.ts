// Dependencies
import { server } from "../src/server";
import supertest from "supertest";

const apiRequest = supertest(server);

describe("API /products", () => {
  let session: string;

  beforeAll(async () => {
    const credentials = {
      email: "sally@tester.de",
      password: "Password123!",
    };

    const res = await apiRequest.post("/auth/login").send(credentials);
    session = res.body.result.session;
  });

  it("GET / should return a list of available products to anyone", async () => {
    const res = await apiRequest.get("/products");
    expect(res.status).toEqual(200);
    expect(res.type).toEqual(expect.stringContaining("json"));
    expect(res.body).toHaveProperty("result");

    expect(Array.isArray(res.body.result)).toBeTruthy();
    expect(res.body.result[0]).toHaveProperty("id");
    expect(res.body.result[0]).toHaveProperty("name");
    expect(res.body.result[0]).toHaveProperty("amountAvailable");
    expect(res.body.result[0]).toHaveProperty("seller");
  });

  it("POST / should create a new product if logged in", async () => {
    const newProduct = {
      name: "My New Product",
      amountAvailable: 500,
    };

    const res = await apiRequest
      .post("/products?s=" + session)
      .send(newProduct);
    expect(res.status).toEqual(201);
    expect(res.type).toEqual(expect.stringContaining("json"));
    expect(res.body).toHaveProperty("result");

    expect(res.body.result).toHaveProperty("id");
    expect(res.body.result).toHaveProperty("name");
    expect(res.body.result.name).toEqual(newProduct.name);
    expect(res.body.result).toHaveProperty("amountAvailable");
    expect(res.body.result.amountAvailable).toEqual(newProduct.amountAvailable);
    expect(res.body.result).toHaveProperty("seller");
  });

  it("POST / should return 400 on ivalid payload", async () => {
    const newProduct = {
      nam: "My New Product",
      amountAvailable: 500,
    };

    const res = await apiRequest
      .post("/products?s=" + session)
      .send(newProduct);
    expect(res.status).toEqual(400);
    expect(res.type).toEqual(expect.stringContaining("json"));
  });

  it("POST / should return 403 if not logged in", async () => {
    const newProduct = {
      nam: "My New Product",
      amountAvailable: 500,
    };

    const res = await apiRequest
      .post("/products")
      .send(newProduct);
    expect(res.status).toEqual(403);
    expect(res.type).toEqual(expect.stringContaining("json"));
  });
});
