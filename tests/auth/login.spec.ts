// Dependencies
import { server } from "../../src/server";
import supertest from "supertest";

const apiRequest = supertest(server);

describe("API /auth/login", () => {
  const API_ENDPOINT = "/auth/login";

  it("POST should return 403 on wrong combination", async () => {
    const credentials = {
      email: "tom@tester.de",
      password: "hallo welt",
    };

    const res = await apiRequest.post(API_ENDPOINT).send(credentials);
    expect(res.status).toEqual(403);
    expect(res.type).toEqual("application/json");
    expect(res.body).toHaveProperty("error");
    expect(res.body.error).toEqual("Forbidden - Wrong credentials");
  });

  it("POST should return session id on valid login", async () => {
    const credentials = {
      email: "tom@tester.de",
      password: "Password123!",
    };

    const res = await apiRequest.post(API_ENDPOINT).send(credentials);
    expect(res.status).toEqual(200);
    expect(res.type).toEqual("application/json");
    expect(res.body).toHaveProperty("result");
    expect(res.body.result).toHaveProperty("session");
  });

  it("POST should return 404 on unknown user", async () => {
    const credentials = {
      email: "unknown@user.de",
      password: "hallo welt",
    };

    const res = await apiRequest.post(API_ENDPOINT).send(credentials);
    expect(res.status).toEqual(404);
    expect(res.type).toEqual("application/json");
    expect(res.body).toHaveProperty("error");
    expect(res.body.error).toEqual("Not Found - Unknown user");
  });

  it("POST should return 409 on missing email", async () => {
    const credentials = {
      password: "hallo welt",
    };

    const res = await apiRequest.post(API_ENDPOINT).send(credentials);
    expect(res.status).toEqual(409);
    expect(res.type).toEqual("application/json");
    expect(res.body).toHaveProperty("error");
    expect(res.body.error).toEqual("Bad Request - Missing parameter(s)");
  });

  it("POST should return 409 on missing email", async () => {
    const credentials = {
      email: "tom@tester.de",
    };

    const res = await apiRequest.post(API_ENDPOINT).send(credentials);
    expect(res.status).toEqual(409);
    expect(res.type).toEqual("application/json");
    expect(res.body).toHaveProperty("error");
    expect(res.body.error).toEqual("Bad Request - Missing parameter(s)");
  });

});
