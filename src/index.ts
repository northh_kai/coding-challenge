// Dependencies
import { server } from "./server";
import dotenv from "dotenv";
import config from "./config";

// Constants
const PORT = config.PORT;

// Setup
server.listen(PORT, () => {
  console.log("INFO: Server listening on port " + PORT);
  console.log("INFO: http://localhost:" + PORT);
});
