//
// Dependencies
//

import dotenv from "dotenv";

//
// Functions / Functionality
//

dotenv.config();

export default {
  PORT: process.env.PORT || "3001",
  SESSION_TTL: process.env.SESSION_TTL || (5 * 60 * 1000).toString(),
};
