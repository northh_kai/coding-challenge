//
// Dependencies
//

import prod from "./prod";
import dev from "./dev";

//
// Types
//

import type { Config } from "../types/Config";

//
// Variables
//

let config: Config;

//
// Functions / Functionality
//

if (process.env.NODE_ENV === "production") {
  config = prod;
} else {
  config = dev;
}

export default config;
