//
// Dependencies
//

import type { Config } from "../types/Config";

//
// Variables
//

const config: Config = {
  PORT: "",
  SESSION_TTL: "",
};

//
// Functions / Functionality
//

if (process.env.PORT) {
  config.PORT = process.env.PORT;
}

if (process.env.SESSION_TTL) {
  config.PORT = process.env.SESSION_TTL;
}

export default config;
