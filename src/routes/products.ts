//
// Dependencies
//
import { Router } from "express";
import requireLogin from "../middlewares/requireLogin";
import {
  NotFoundError,
  BadRequestError,
  ForbiddenError,
} from "../utils/Errors";

import productStore from "../store/productStore";

//
// Types
//
import type { Request, Response } from "express";
import { NewProduct } from "../types";

//
// Variables
//
const router = Router();

//
// Helper functions
//

//
// Routes
//

router.get("/products", (req: Request, res: Response) => {
  const products = productStore.getAll();
  return res.status(200).json({ result: products });
});

router.post("/products", requireLogin, (req: Request, res: Response) => {
  try {
    // TODO check req.body before creating new product

    let newProduct: NewProduct = {
      id: 101,
      name: req.body.name,
      amountAvailable: req.body.amountAvailable,
      seller: req.user?.id,
    };

    const createdProduct = productStore.add(newProduct);
    return res.status(201).json({ result: createdProduct });
  } catch (err) {
    console.error(err);
    if (err instanceof BadRequestError) {
      return res.status(err.statusCode).json({ error: "Invalid Request" });
    }

    return res.status(500).json({ error: "Internal Server Error" });
  }
});

export default router;
