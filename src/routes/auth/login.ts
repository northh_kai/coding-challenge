//
// Dependencies
//
import { Router } from "express";
import crypto from "crypto";
import bcrypt from "bcrypt";
import {
  NotFoundError,
  BadRequestError,
  ForbiddenError,
} from "../../utils/Errors";
import config from "../../config";
import { addSession, findSession } from "../../store/sessionStore";

import { findUserByEmail } from "../../store/userStore";

//
// Types
//
import type { Request, Response } from "express";
import type { Session, User } from "../../types";

//
// Variables
//
const router = Router();

//
// Helper functions
//

/**
 * Return user from In-Memory-Database or throws errors
 *
 * @throws Error "User not found"
 *
 * @param email string
 * @returns User
 */
function getUser(email: string): User {
  let user = findUserByEmail(email);

  if (user === null) {
    throw new NotFoundError("User not found");
  }

  return user;
}

/**
 * Checks if required parameters are present in request object.
 *
 * @throws BadRequestError
 *
 * @param req
 */
function checkParameters(req: Request) {
  if (req.body.email === undefined) {
    throw new BadRequestError("Parameter 'email' missing");
  }

  if (req.body.password === undefined) {
    throw new BadRequestError("Parameter 'password' missing");
  }
}

/**
 * Checks if password matches with passwordHash.
 *
 * @throws ForbiddenError
 *
 * @param user
 * @param password
 */
async function checkPassword(user: User, password: string) {
  const passwordMatches = await bcrypt.compare(password, user.passwordHash);

  if (!passwordMatches) {
    throw new ForbiddenError("Wrong credentials");
  }
}

function generateSession(): string {
  const hashValue = Date.now.toString() + Math.random();
  let sessionHash = crypto.createHash("md5").update(hashValue).digest("hex");

  return sessionHash;
}

//
// Routes
//

router.post("/auth/login", async (req: Request, res: Response) => {
  try {
    checkParameters(req);
    const user = getUser(req.body.email);
    await checkPassword(user, req.body.password);

    const sessionHash = generateSession();

    // create new session
    addSession({
      id: sessionHash,
      email: req.body.email,
      validUntil: new Date(Date.now() + parseInt(config.SESSION_TTL)),
    });

    res.status(200);
    res.json({ result: { session: sessionHash } });
  } catch (err) {
    if (err instanceof ForbiddenError) {
      res.status(403);
      res.json({ error: "Forbidden - Wrong credentials" });
    }

    if (err instanceof NotFoundError) {
      res.status(404);
      res.json({ error: "Not Found - Unknown user" });
    }

    if (err instanceof BadRequestError) {
      res.status(409);
      res.json({ error: "Bad Request - Missing parameter(s)" });
    }
  }
});

export default router;
