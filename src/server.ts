// Dependencies
import express from "express";
import type { Request, Response } from "express";
import bodyParser from "body-parser";
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";

// Routes
import loginRoute from "./routes/auth/login";
import productRoutes from "./routes/products";

// Middleware
import requireLogin from "./middlewares/requireLogin";
import getUserBySession from "./middlewares/getUserBySession";

// Setup
const app = express();
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());

if (process.env.NODE_ENV !== "test") {
  app.use(morgan("combined"));
}

app.use(getUserBySession);

// Routes

app.use(loginRoute);
app.use(productRoutes);

app.get("/", (req: Request, res: Response) => {
  res.json({ result: "I am a vending machine" });
});

app.get("/protected", requireLogin, (req: Request, res: Response) => {
  res.json({ result: "Protected page" });
});

export const server = app;
