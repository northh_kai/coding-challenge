import type { User } from "../types";
import { NotFoundError } from "../utils/Errors";

let users: Array<User> = [
  {
    id: 1,
    email: "tom@tester.de",
    passwordHash:
      "$2b$10$VWxNnt0YdBFjiatujkM5.ui1yjlBbqEdXN5UyASQ71PtG94J19zWm",
    deposit: 0,
    role: "buyer",
  },
  {
    id: 2,
    email: "sally@tester.de",
    passwordHash:
      "$2b$10$VWxNnt0YdBFjiatujkM5.ui1yjlBbqEdXN5UyASQ71PtG94J19zWm",
    deposit: 0,
    role: "seller",
  },
];

function findUserByEmail(email: string): User {
  const user = users.filter((u) => u.email === email);

  if (user.length > 0) {
    return user[0];
  } else {
    throw new NotFoundError("User not found.");
  }
}

export { findUserByEmail };
