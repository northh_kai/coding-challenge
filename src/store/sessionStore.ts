import type { Session } from "../types";
import { NotFoundError } from "../utils/Errors";

let sessions: Array<Session> = [];

function addSession(session: Session) {
  sessions.push(session);
}

function findSession(sessionHash: string): Session {
  const session = sessions.filter((s) => s.id === sessionHash);

  if (session.length > 0) {
    return session[0];
  } else {
    throw new NotFoundError("Session not found");
  }
}

export { addSession, findSession };
