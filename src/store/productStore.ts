import type { Product, NewProduct } from "../types";
import { BadRequestError } from "../utils/Errors";

let products: Array<Product> = [
  {
    id: 1,
    name: "Snickers",
    amountAvailable: 25,
    seller: 2,
  },
  {
    id: 2,
    name: "Mars",
    amountAvailable: 23,
    seller: 2,
  },
  {
    id: 3,
    name: "Twix",
    amountAvailable: 12,
    seller: 2,
  },
  {
    id: 4,
    name: "Duplo",
    amountAvailable: 7,
    seller: 2,
  },
];

function getAll(): Array<Product> {
  return products;
}

function add(product: NewProduct): Product {
  if (
    product &&
    product.id &&
    product.name &&
    product.amountAvailable &&
    product.seller
  ) {
    const newProduct: Product = {
      id: product.id,
      name: product.name,
      amountAvailable: product.amountAvailable,
      seller: product.seller,
    };

    products.push(newProduct);
    return newProduct;
  } else {
    throw new BadRequestError("Invalid product");
  }
}

export default { getAll, add };
