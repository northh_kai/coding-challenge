type Product = {
  id: number;
  name: string;
  amountAvailable: number; // multiple of 5
  seller: number;
};

type NewProduct = {
  id?: number;
  name?: string;
  amountAvailable?: number;
  seller?: number;
};

export { Product, NewProduct };
