type Session = {
  id: string;
  email: string;
  validUntil: Date;
};

export { Session };
