type User = {
  id: number;
  email: string;
  passwordHash: string;
  deposit: number;
  role: "seller" | "buyer" | string;
};

export { User };
