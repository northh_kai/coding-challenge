type Coin = {
  value: 5 | 10 | 20 | 50 | 100;
};

export { Coin };
