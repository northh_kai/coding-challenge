type Config = {
  PORT: string;
  SESSION_TTL: string;
};

export { Config };
