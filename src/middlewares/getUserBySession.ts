//
// Dependencies
//

import { NotFoundError, ForbiddenError } from "../utils/Errors";

//
// Types
//

import type { Session, User } from "../types";
import type { Request, Response, NextFunction } from "express";
import { findSession } from "../store/sessionStore";
import { findUserByEmail } from "../store/userStore";

//
// Functions
//

async function getUserBySession(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const sessionId = req.query.s;

    if (sessionId) {
      const session: Session = findSession(sessionId.toString());

      if (session.validUntil.getTime() < Date.now()) {
        throw new ForbiddenError("Session not valid anymore");
      }

      // append user object to request
      const user: User = findUserByEmail(session.email);
      req.user = user;
    }
  } catch (err) {
    if (err instanceof ForbiddenError || err instanceof NotFoundError) {
      res.status(403).json({ error: "Forbidden - Invalid session" });
    } else {
      res.status(500).json({ error: "Internal Server Error" });
    }

    return res;
  }

  next();
}

export default getUserBySession;
