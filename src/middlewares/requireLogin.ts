//
// Types
//

import type { Request, Response, NextFunction } from "express";

//
// Functions
//

function requireLogin(req: Request, res: Response, next: NextFunction) {
  if (req.user === undefined) {
    res.status(403).json({ error: "Forbidden - Access denied" });
    return res;
  }

  next();
}

export default requireLogin;
